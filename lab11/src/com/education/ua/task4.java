package com.education.ua;

import java.sql.*;

public class task4 {
    public static void main(String[] args) throws SQLException {
        Connection connection = SQLDatabaseConnection.connect();
        Statement statement = connection.createStatement();

        String insertSQL = "INSERT INTO lab11.product (name, count) VALUES (?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(insertSQL);

        for (int i = 11; i <= 15; i++) {
            preparedStatement.setString(1, "  " + i);
            preparedStatement.setInt(2, 25 + i);
            preparedStatement.addBatch();
        }

        preparedStatement.executeBatch();


        String selectByAgeSQL = "SELECT * FROM lab11.product WHERE count > ?";
        PreparedStatement selectByAgeStatement = connection.prepareStatement(selectByAgeSQL);
        selectByAgeStatement.setInt(1, 25); // :         25

        ResultSet resultSet = selectByAgeStatement.executeQuery();

        System.out.println("       25:");
        while (resultSet.next()) {
            int productId = resultSet.getInt("id");
            String productName = resultSet.getString("name");
            int productCount = resultSet.getInt("count");

            System.out.println("ID: " + productId + ", Name: " + productName + ", Count: " + productCount);
        }

        //      
        String deleteAllSQL = "DELETE FROM lab11.product";
        statement.executeUpdate(deleteAllSQL);

        //  
        resultSet.close();
        selectByAgeStatement.close();
        preparedStatement.close();
        statement.close();
        connection.close();
    }
}

