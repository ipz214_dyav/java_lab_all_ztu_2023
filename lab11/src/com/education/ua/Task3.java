package com.education.ua;

import java.sql.*;

public class Task3 {
    public static void main(String[] args)throws SQLException {
        Connection connection = SQLDatabaseConnection.connect();
        Statement state = connection.createStatement();
        String createTableSQL = "CREATE TABLE IF NOT EXISTS product (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(50), count INT)";
        state.executeUpdate(createTableSQL);

        String insertSQL = "INSERT INTO lab11.product (name, count) VALUES (?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(insertSQL);

        for (int i = 1; i <= 10; i++) {
            preparedStatement.setString(1, "  " + i);
            preparedStatement.setInt(2, 20 + i);
            preparedStatement.addBatch();
        }

        preparedStatement.executeBatch();

        String selectSQL = "SELECT * FROM lab11.product";
        ResultSet resultSet = state.executeQuery(selectSQL);

        while (resultSet.next()) {
            int productId = resultSet.getInt("id");
            String productName = resultSet.getString("name");
            int productCount = resultSet.getInt("count");

            System.out.println("ID: " + productId + ", Name: " + productName + ", Count: " + productCount);
        }

        resultSet.close();
        preparedStatement.close();
        state.close();
        connection.close();

    }
}
