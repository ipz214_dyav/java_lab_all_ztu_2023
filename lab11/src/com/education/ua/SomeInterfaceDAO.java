package com.education.ua;

import java.sql.SQLException;
import java.util.List;

public interface SomeInterfaceDAO<T> {
    T getById(int id) throws SQLException;
    List<T> getAll() throws SQLException;
    void insert(T obj);
    void update(int id ,T obj);
    void delete(int id);
}
