<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    String error = request.getParameter("error");
    if (error != null) {
%>
<h1><%= error %>
</h1>
<%
} else {
        String username = (String) session.getAttribute("username");
        String loggedInUser = (String) session.getAttribute("loggedInUser");

        if (username != null && loggedInUser != null) {
%>
<h1>Congratulations <%= username %>! Authorization was successful</h1>
<%
            }
        }
%>
</body>
</html>
