package com.eeducation.ztu.java_lab_14;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;


@WebServlet(name = "firstServlet", value = "/firstServlet")
public class FirstServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    public void init() throws ServletException {
        super.init();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String paramName = request.getParameter("name");
        if(!paramName.equals(""))
        {
            Cookie cookie = new Cookie("name", paramName);
            response.addCookie(cookie);
            response.sendRedirect("get.jsp");
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));


        Map<String, String> jsonMap = objectMapper.readValue(jsonString, new TypeReference<Map<String, String>>() {});
        String username = jsonMap.get("username");
        String password = jsonMap.get("password");
        

        if (username != null && password != null) {
            if ("user".equals(username) && "password".equals(password)) {
                HttpSession session = request.getSession();
                session.setAttribute("loggedInUser", username);
                session.setAttribute("username", username);
                session.setAttribute("password", password);
                response.sendRedirect("post.jsp");
            } else {
                response.sendRedirect("post.jsp?error=invalid_credentials");
            }
        } else {
            response.sendRedirect("post.jsp?error=invalid_credentials");
        }
    }


}
