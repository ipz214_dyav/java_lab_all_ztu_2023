package com.education.ua;

import java.io.*;

public class Task4 {
    public static void main(String[] args) {
        try (FileInputStream fileInputStream = new FileInputStream("./directory_for_files/sourceTask4.png");
             FileOutputStream fileOutputStream = new FileOutputStream("./directory_for_files/copyTask4.png")) {
            byte[] buffer = new byte[1024];
            int bytesRead;

            while ((bytesRead = fileInputStream.read(buffer)) != -1) {
                fileOutputStream.write(buffer, 0, bytesRead);
            }

            System.out.println("��������� ���������.");
        } catch (IOException e) {
            System.err.println("�������: " + e.getMessage());
        }
    }
}
