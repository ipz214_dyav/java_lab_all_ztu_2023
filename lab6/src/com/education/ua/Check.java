package com.education.ua;

import java.text.NumberFormat;
import java.util.*;

public class Check {
    public Formatter printCheck(Locale locale){

        ResourceBundle messages = ResourceBundle.getBundle("data", locale);
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
        Formatter formatter = new Formatter();
        List<Goods> products = new ArrayList<>();
        String purchaseDateTime = "28.03.2019 13:25:12";
        double total = 0;

        products.add(new Goods("������", "Ƴ����� ����", 1500.78));
        products.add(new Goods("��������", "�������� ����", 450.50));
        products.add(new Goods("�������", "������", 1200.00));
        products.add(new Goods("��������", "�����������", 8999.99));
        products.add(new Goods("�����", "�����", 25.99));
        products.add(new Goods("���", "��������", 40.25));
        products.add(new Goods("�������", "�����������", 2500.00));
        products.add(new Goods("�������", "�����", 99.99));
        products.add(new Goods("������", "�����", 899.00));

        formatter.format(messages.getString("datetime") + ": %22s%n", purchaseDateTime);
        formatter.format(messages.getString("line") + "%n");
        formatter.format("%-2s %-14s %-19s %2s%n", messages.getString("number"), messages.getString("product"),
                messages.getString("category"), messages.getString("price"));
        formatter.format(messages.getString("line") + "%n");

        for (Goods product : products) {
            formatter.format("%-2d. %-13s %-18s %s %n", product.getId(), product.getName(), product.getCategory(), currencyFormatter.format( product.getPrice()));
            total += product.getPrice();
        }

        String totalString = currencyFormatter.format(total);
        formatter.format(messages.getString("line") + "%n");
        formatter.format("%-36s %s %n", messages.getString("total"), totalString);

       return formatter;
    }
}
