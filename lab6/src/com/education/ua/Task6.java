package com.education.ua;

import java.io.File;
import java.io.IOException;

public class Task6 {
    public static void main(String[] args) {
        File innerDirectory = new File("./directory_for_files/inner_directory");
        if (!innerDirectory.exists()) {
            boolean directoryCreated = innerDirectory.mkdir();
            if (directoryCreated) {
                System.out.println("����� 'inner_directory' ���� ��������.");
            } else {
                System.out.println("�� ������� �������� ����� 'inner_directory'.");
                return;
            }
        }

        try {
            System.out.println("���������� ���� �������� �����: " + innerDirectory.getAbsolutePath());

            File parentDirectory = innerDirectory.getParentFile();
            if (parentDirectory != null) {
                System.out.println("��'� ���������� ��������: " + parentDirectory.getName());
            } else {
                System.out.println("�� �������� ���������.");
            }

            File file1 = new File(innerDirectory, "file1.txt");
            File file2 = new File(innerDirectory, "file2.txt");

            System.out.println( file1.createNewFile() + "\n" +  file2.createNewFile());

            if (file1.delete()) {
                System.out.println("���� 'file1.txt' ���� ��������.");
            } else {
                System.out.println("�� ������� �������� ���� 'file1.txt'.");
            }

            File renamedDirectory = new File("./directory_for_files/renamed_inner_directory");
            if (innerDirectory.renameTo(renamedDirectory)) {
                System.out.println("����� 'inner_directory' ������������� � 'renamed_inner_directory'.");
            } else {
                System.out.println("�� ������� ������������� �����.");
            }

            File directoryForFiles = new File("./directory_for_files");
            File[] contents = directoryForFiles.listFiles();
            if (contents != null) {
                for (File item : contents) {
                    String type = item.isDirectory() ? "�����" : "����";
                    System.out.println("��'�: " + item.getName() + ", �����: " + item.length() + " ����, ���: " + type);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
