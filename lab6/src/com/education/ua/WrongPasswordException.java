package com.education.ua;

public class WrongPasswordException extends Throwable {
    public WrongPasswordException() {
        super("Неправильний пароль");
    }

    public WrongPasswordException(String message) {
        super(message);
    }
}
