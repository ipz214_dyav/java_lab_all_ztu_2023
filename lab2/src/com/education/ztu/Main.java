package com.education.ztu;

import java.util.Arrays;

enum Location{
    Kiev,Zhytomyr;
    Location valueof(String value){
        return Location.Zhytomyr;
    }
}

enum Gender{
    Male,Female;
}

class Car{
    String brand;
    Engine engine;

    Car(String brand){
        this.brand=brand;
    }

    class Engine{
        boolean engineWorks;
        void startEngine(){
            engineWorks=true;
        }
        void stopEngine(){
            engineWorks=false;
        }
    }
}

interface Human{
    String fullInfo="";

    abstract void sayAge();
    void sayGender();
    void sayLocation();
    void sayName();
    void whoIAm();
}

 abstract class Person implements Human{
    int age;
    String firstName;
    String lastName;
    String fullInfo;
    Gender gender;
    Location location;

    Person(){
        this.firstName="Yaroslav";
        this.lastName="Dubnitskiy";
        this.fullInfo=this.firstName+this.lastName;
        this.age=20;
    }

    Person(String firstName,String fullInfo,String lastName,int age,Gender gender,Location location){
        this.firstName=firstName;
        this.lastName=lastName;
        this.fullInfo=fullInfo;
        this.age=age;
        this.gender=gender;
        this.location=location;
    }

    abstract void getOccupation();
    @Override
    public void sayAge(){};
    @Override
    public void sayGender(){};
    @Override
    public void sayLocation(){};
    @Override
    public void sayName(){};
}

class Teacher extends Person{
    Car car;
    String fullInfo;
    String subject;
    String university;
    static int counter;

    Teacher(String fullInfo,String university,Car car){
        this.fullInfo=fullInfo;
        this.university=university;
        this.car=car;
    }

    Teacher(String fullInfo,String university,Car car, String subject,String firstName,Location location,Gender gender){
        this.fullInfo=fullInfo;
        this.university=university;
        this.car=car;
        this.subject=subject;
        this.gender=gender;
        this.location=location;
        this.firstName=firstName;
    }

    @Override
    void getOccupation() {
        System.out.println("Occupation");
    }
    @Override
    public void whoIAm() {
        System.out.println("whoIAm");
    }
    static int showCounter(){
        return counter;
    }
}

class Student extends Person{
    int course;
    String speciality;
    String fullInfo;
    String university;
    static int counter;

    Student(String fullInfo,String university,int course){
        this.fullInfo=fullInfo;
        this.university=university;
        this.course=course;
    }

    Student(String fullInfo,String university, String subject,String firstName,Location location,Gender gender,int course,int age){
        this.fullInfo=fullInfo;
        this.university=university;
        this.gender=gender;
        this.location=location;
        this.firstName=firstName;
        this.course=course;
        this.age=age;
    }

    @Override
    void getOccupation() {
        System.out.println("Occupation");
    }
    @Override
    public void whoIAm() {
        System.out.println("whoIAm");
    }
    static int showCounter(){
        return counter;
    }
}

class Employee extends Person{
    String fullInfo;
    Car car;
    String company;
    String position;
    static int counter;

    Employee(String fullInfo,String university,Car car){
        this.fullInfo = fullInfo;
        this.company = company;
        this.car = car;
    }

    Employee(String fullInfo,String company, String position,String firstName,Location location,Gender gender,int age,Car car){
        this.fullInfo=fullInfo;
        this.position=position;
        this.gender=gender;
        this.location=location;
        this.firstName=firstName;
        this.company=company;
        this.age=age;
        this.car=car;
    }

    @Override
    void getOccupation() {
        System.out.println("Occupation");
    }
    @Override
    public void whoIAm() {
        System.out.println("whoIAm");
    }
    static int showCounter(){
        return counter;
    }
}

class Operation{
    String[] print(String ...args){
        return args;
    }
}

public class Main {
    public Main() {
    }

    public static void main(String[] args) {
        Car Audi = new Car("Audi");
        Car Porshe = new Car("Porshe");
        Teacher teacher = new Teacher("nice teacher","ZHTK",Audi,"PE","Pavel",Location.Zhytomyr,Gender.Male);
        System.out.println(teacher.car.brand);

        Student Yaroslav = new Student("Yaroslav Dubnitskiy","ZHPT",3);
        System.out.println(Yaroslav.fullInfo);

        Employee klerk = new Employee("Klark","Big office",Porshe);
        System.out.println(klerk.fullInfo);

        Operation oper = new Operation();
        System.out.println(Arrays.toString(oper.print(Location.Zhytomyr.name(), Location.Kiev.name())));
    }
}
