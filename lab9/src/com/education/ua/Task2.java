package com.education.ua;

import java.util.regex.*;

public class Task2 {

    public static void main(String[] args) {
        String text = Text.text;
        findNumberPhone(text);
        findEmail(text);
        replaceDateOfBirthday(text);
        replaceJob(text);
    }


    public static void findNumberPhone(String text){
        String stringPattern = "\\+380\\d{9}";
        Pattern pattern = Pattern.compile(stringPattern);
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            System.out.println(text.substring(matcher.start(), matcher.end()));
        }
    }


    public static void findEmail(String text){
        String stringPattern = "\\b[\\w.%+-]+@[\\w.-]+\\.[A-Za-z]{2,}\\b";
        Pattern pattern = Pattern.compile(stringPattern);
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            System.out.println(text.substring(matcher.start(), matcher.end()));
        }
    }

    public static void replaceDateOfBirthday(String text){
        String stringPattern = "(\\d{4})-(\\d{2})-(\\d{2})";
        String formattedDate = text.replaceAll(stringPattern, "$3.$2.$1");
        System.out.println(formattedDate);
    }

    public static void replaceJob(String text){
        String regex = "������: (.+?)\n";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);

        int count = 0;
        StringBuffer result = new StringBuffer();
        while (matcher.find() && count < 3) {
            matcher.appendReplacement(result, "������: ���������\n");
            count++;
        }
        matcher.appendTail(result);

        System.out.println("�������� ����������:");
        System.out.println(result.toString());
    }
}
