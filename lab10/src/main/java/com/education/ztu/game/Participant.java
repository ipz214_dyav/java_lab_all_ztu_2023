package com.education.ztu.game;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

/**
 * Абстрактний клас Participant, який реалізує Cloneable, Comparable та Serializable.
 * Цей клас є базовим класом для представлення учасників.
 */
public abstract class Participant implements Cloneable, Comparable<Participant>, Serializable {

    /** Унікальний ідентифікатор версії серіалізації */
    @Serial
    private static final long serialVersionUID = -1649126398712636016L;

    /** Ім'я учасника */
    private String name;

    /** Вік учасника (позначення transient означає, що це поле не буде серіалізоване) */
    private transient int age;

    /**
     * Конструктор класу Participant.
     *
     * @param name ім'я учасника
     * @param age вік учасника
     */
    public Participant(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Participant() {}

    /**
     * Метод для отримання імені учасника
     * @return
     * повертає name
     */
    public String getName() {
        return name;
    }

    /**
     * Метод для отримання віку учасника
     * @return
     * повертає age
     */
    public int getAge() {
        return age;
    }

    /**
     * Метод для встановлення імені учасника
     * @param name
     * name - ім'я учасника
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Метод для встановлення віку учасника
     * @param age
     * age - вік учасника
     */
    public void setAge(int age) {
        this.age = age;
    }

    /** Перевизначений метод clone для створення копії об'єкта Participant */
    @Override
    public Participant clone() throws CloneNotSupportedException {
        return (Participant) super.clone();
    }

    /** Перевизначений метод equals для порівняння двох об'єктів типу Participant */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Participant other = (Participant) obj;

        return this.age == other.age && Objects.equals(this.name, other.name);
    }

    /** Перевизначений метод hashCode для створення хеш-коду об'єкта Participant */
    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    /** Перевизначений метод compareTo для порівняння об'єктів Participant за іменем */
    @Override
    public int compareTo(Participant o) {
        return this.name.compareTo(o.name);
    }

    /** Перевизначений метод toString для представлення об'єкта Participant у вигляді рядка */
    @Override
    public String toString() {
        return "Participant{" +
                "name=" + this.name +
                ", age='" + this.age + '\'' +
                '}';
    }
}
