package com.education.ztu.game;

import java.io.Serial;
import java.io.Serializable;
import java.util.Comparator;


public class AgeComparator implements Comparator<Participant>, Serializable {


    @Serial
    private static final long serialVersionUID = 2896644851502694383L;

    public AgeComparator() {}

    /**
     * Порівнює вік двох учасників.
     *
     * @param o1 перший учасник
     * @param o2 другий учасник
     * @return результат порівняння: від'ємне число, якщо o1 менше o2;
     *         0, якщо вони рівні; додатне число, якщо o1 більше o2
     */
    @Override
    public int compare(Participant o1, Participant o2) {
        return Integer.compare(o1.getAge(), o2.getAge());
    }
}
