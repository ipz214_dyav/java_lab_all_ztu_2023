package com.education.ztu.game;

//import org.slf4j.*;

import java.io.Serial;
import java.io.Serializable;


public class Game implements Serializable {

    @Serial
    private static final long serialVersionUID = -2801498872002612484L;

    public Game() {}

    /**
     * Вхідна точка програми, яка викликається при запуску програми.
     * Ініціалізує логера та відображає повідомлення різних рівнів через логера.
     *
     * @param args аргументи командного рядка, передані при запуску програми
     */
    public static void main(String[] args) {
//         Ініціалізація логера для логування подій гри
//        Logger logger = LoggerFactory.getLogger("gameLogger");
//
////         Відлагодження та відображення повідомлень різних рівнів через логера
//        logger.debug("Debug message");
//        logger.info("Info message");
//        logger.warn("Warning message");
//        logger.error("Error message");

        Schoolar schoolar1 = new Schoolar("Igor", 13);
        Schoolar schoolar2 = new Schoolar("kolya", 15);
        Schoolar schoolar3 = new Schoolar("Mariya", 51);

        Schoolar schoolar4 = new Schoolar("Sergey", 12);
        Schoolar schoolar5 = new Schoolar("Ina", 14);
        Schoolar schoolar6 = new Schoolar("Ivan", 39);

        // Створення команд для учасників гри
        Team<Schoolar> scollarTeam = new Team<>("Dragon");
        Team<Schoolar> scollarTeam2 = new Team<>("Rozumnyky");

        scollarTeam.addNewParticipant(schoolar1);
        scollarTeam.addNewParticipant(schoolar2);
        scollarTeam.addNewParticipant(schoolar3);

        scollarTeam2.addNewParticipant(schoolar4);
        scollarTeam2.addNewParticipant(schoolar5);
        scollarTeam2.addNewParticipant(schoolar6);

        scollarTeam.playWith(scollarTeam2);
    }
}
