package com.education.ztu;

import com.education.ztu.game.*;

import java.util.Comparator;

public class Main2 {
    public static void main(String[] args) {
        Schoolar schoolar1 = new Schoolar("Kolya", 15);
        Schoolar schoolar2 = new Schoolar("Denis", 15);
        Schoolar schoolar3 = new Schoolar("Mariya", 51);

        Schoolar schoolar4 = new Schoolar("Sergey", 12);
        Schoolar schoolar5 = new Schoolar("Olga", 14);
        Schoolar schoolar6 = new Schoolar("Ivan", 39);

        Team<Schoolar> scollarTeam = new Team<>("Snake");
        Team<Schoolar> scollarTeam2 = new Team<>("Car");

        AgeComparator ageComparator = new AgeComparator();
        Comparator<Participant> comparator = PriorityComparator.getNameAgeComparator();

        scollarTeam.addNewParticipant(schoolar1);
        scollarTeam.addNewParticipant(schoolar2);
        scollarTeam.addNewParticipant(schoolar3);

        scollarTeam2.addNewParticipant(schoolar4);
        scollarTeam2.addNewParticipant(schoolar5);
        scollarTeam2.addNewParticipant(schoolar6);

        scollarTeam.playWith(scollarTeam2);

        System.out.println("schoolar1.compareTo(schoolar2):\n" + schoolar1.compareTo(schoolar2));
        System.out.println("schoolar2.compareTo(schoolar3):\n" + schoolar2.compareTo(schoolar3));


        System.out.println("ageComparator.compare(schoolar1,schoolar2):\n" + ageComparator.compare(schoolar1,schoolar2));
        System.out.println("comparator.compare(schoolar1,schoolar2):\n" + comparator.compare(schoolar1,schoolar2));

    }
}
