package com.education.ztu.game;

import java.util.Comparator;

public class PriorityComparator{

    public static Comparator<Participant> getNameAgeComparator() {
        return Comparator.comparing(Participant::getName)
                .thenComparingInt(Participant::getAge);
    }
}
