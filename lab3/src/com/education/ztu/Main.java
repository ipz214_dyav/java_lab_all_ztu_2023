package com.education.ztu;

import com.education.ztu.game.Schoolar;
import com.education.ztu.game.Team;

public class Main {

    public static void main(String[] args) throws CloneNotSupportedException {
        Schoolar schoolar1 = new Schoolar("Den", 13);
        Schoolar schoolar2 = new Schoolar("Olya", 15);
        Schoolar schoolar3 = new Schoolar("Markiz", 51);

        Schoolar schoolar4 = new Schoolar("Sergey", 12);
        Schoolar schoolar5 = new Schoolar("Olga", 14);
        Schoolar schoolar6 = (Schoolar) schoolar5.clone();


        Team<Schoolar> scollarTeam = new Team<>("Fenix");
        Team<Schoolar> scollarTeam2 = new Team<>("Snake");

        scollarTeam.addNewParticipant(schoolar1);
        scollarTeam.addNewParticipant(schoolar2);
        scollarTeam.addNewParticipant(schoolar3);

        scollarTeam2.addNewParticipant(schoolar4);
        scollarTeam2.addNewParticipant(schoolar5);
        scollarTeam2.addNewParticipant(schoolar6);

        scollarTeam.playWith(scollarTeam2);

        Team<Schoolar> scoolarTeamClone = scollarTeam2.clone();
        scoolarTeamClone.getParticipants().stream()
                .map(Object::toString)
                .forEach(System.out::println);
        System.out.println("HashCode:\n" + schoolar5.hashCode());
        System.out.println(schoolar6.hashCode());
        System.out.println("Equals schoolar5 and schoolar6:\n" +schoolar5.equals(schoolar6));
        System.out.println(schoolar5);

    }
}
