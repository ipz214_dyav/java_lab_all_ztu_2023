package com.education.ztu;

public class Goods {
    private final int id;
    private String name;
    private String category;
    private double price;
    private static int autoIncrement;

    public Goods(String name, String category, double price) {
        this.id = autoIncrement;
        this.name = name;
        this.category = category;
        this.price = price;
        autoIncrement++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }
}
