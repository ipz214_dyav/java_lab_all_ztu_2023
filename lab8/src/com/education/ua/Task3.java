package com.education.ua;

import java.util.L

import com.education.ua.SQLDatabaseConnection;ocale;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Task3 {
    public static void main(String[] args) {
        Predicate<String> predicate = (s)->{
            try
            {
                int i = Integer.parseInt(s.trim());
            }
            catch (NumberFormatException nfe)
            {
                System.out.println("NumberFormatException: " + nfe.getMessage());
            }
            return true;
        };
        System.out.println(predicate.test("123"));

        //Consumer

        Consumer<String> startClass = (time) -> System.out.println("   " + time);
        Consumer<String> endClass = (time) -> System.out.println("   " + time);
        Consumer<String> schedule = startClass.andThen(endClass);

        schedule.accept("8:30");

        //Supplier

        Supplier<String> supplier = ()->" ".toUpperCase(Locale.ROOT);
        System.out.println(supplier.get());

        //Function<String, Integer>

        Function<String, Integer> function = (s)->{
            int sum = 0;
            String[] a = s.split(" ");
            for (String number:a) {
                sum += Integer.parseInt(number);
            }
            return sum;
        };

        System.out.println(function.apply("1 2 3 4 5 6"));
    }
}

