package com.education.ua;

import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

public class Task2Lamda {
    public static void main(String[] args) {
        Printable printable = (Graphics graphics, PageFormat pageFormat, int pageIndex) -> {
            if (pageIndex > 0) {
                return Printable.NO_SUCH_PAGE;
            }
            Graphics2D g2d = (Graphics2D) graphics;
            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
            String textToPrint = " )";

            Font font = new Font("Arial", Font.PLAIN, 12);
            g2d.setFont(font);
            g2d.drawString(textToPrint, 100, 100);
            return Printable.PAGE_EXISTS;
        };

        PrinterJob job = PrinterJob.getPrinterJob();
        job.setPrintable(printable);

        try {
            job.print();
        } catch (PrinterException e) {
            e.printStackTrace();
        }
    }
}

