package com.education.ua;

public class Task4 implements Runnable{
    private static volatile int result = 1;

    private synchronized static void incrementResult() {
        System.out.print(result + " ");
        result++;
    }

    @Override
    public void run() {
        for (int i = 0; i < 40; i++) {
            incrementResult();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Task4 arithmeticProgression = new Task4();

        Thread thread1 = new Thread(arithmeticProgression);
        Thread thread2 = new Thread(arithmeticProgression);
        Thread thread3 = new Thread(arithmeticProgression);

        thread1.start();
        thread2.start();
        thread3.start();
    }
}


