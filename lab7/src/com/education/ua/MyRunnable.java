package com.education.ua;

public class MyRunnable implements Runnable {

    @Override
    public void run() {
        try {
            for (int i = 0; i < 1000; i++) {
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println(" .  !!!");
                    return;
                }
                if (i % 10 == 0) {
                    System.out.println(": " + i);
                }
            }
        } catch (Exception e) {
            System.out.println(" : " + e.getMessage());
        }

    }
}

