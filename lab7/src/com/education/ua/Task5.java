package com.education.ua;

public class Task5 implements Runnable {
    private static int result = 1;



    @Override
    public void run() {
        for (int i = 0; i < 40; i++) {

            synchronized (Task5.class){
                System.out.print(result + " ");
                result++;
            }
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Task5 arithmeticProgression = new Task5();

        Thread thread1 = new Thread(arithmeticProgression);
        Thread thread2 = new Thread(arithmeticProgression);
        Thread thread3 = new Thread(arithmeticProgression);

        thread1.start();
        thread2.start();
        thread3.start();
    }
}
