package com.education.ua;

import java.util.Random;

public class SumOfDigitsInArray implements Runnable {


    @Override
    public void run() {
        int[] array = new int[1000000];

        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(1000);
        }

        long totalSum = calculateTotalSumOfDigits(array);
        System.out.println("    : " + totalSum);
    }

    private static long calculateTotalSumOfDigits(int[] array) {
        long sum = 0;
        for (int number : array) {
            sum += sumOfDigits(number);
        }
        return sum;
    }

    private static int sumOfDigits(int number) {
        int sum = 0;
        while (number != 0) {
            sum += number % 10;
            number /= 10;
        }
        return sum;
    }

}
