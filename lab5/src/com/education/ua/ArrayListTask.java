package com.education.ua;

import java.util.*;

public class ArrayListTask {

 public static void main(String[] args) {

  ArrayList<Product> arrayList = new ArrayList<>();
  ArrayList<Product> arrayList2 = new ArrayList<>();

  Product product1 = new Product("������", 16.50, "���������");
  Product product2 = new Product("���", 23.10, "���������");
  Product product3 = new Product("�������", 19.40, "���������");
  Product product4 = new Product("���", 22.50, "������� ���");
  Product product5 = new Product("������� � ������", 10.0, "������� ���");

  arrayList.add(product1);
  arrayList.add(product2);
  arrayList.add(product3);
  arrayList.add(product4);
  arrayList2.add(product5);
  arrayList2.add(product2);
  arrayList2.add(product3);

  arrayList.addAll(arrayList2);

  System.out.println("arrayList.get(1): " + arrayList.get(1));

  System.out.println("arrayList.indexOf(product1): " + arrayList.indexOf(product1));

  System.out.println("arrayList.lastIndexOf(product2): " + arrayList.lastIndexOf(product2));

  System.out.println("Iterator");
  Iterator<Product> iter = arrayList.iterator();
  while (iter.hasNext()) {
   System.out.println(iter.next() + " ");
  }

  System.out.println("ListIterator");
  ListIterator<Product> listIter = arrayList.listIterator();
  while (listIter.hasNext()) {
   System.out.println(listIter.next() + " ");
  }

  System.out.println("arrayList.remove(product3): " + arrayList.remove(product3));

  System.out.println("arrayList.set(3, product3): " + arrayList.set(3, product3));

  System.out.println("����������");
  arrayList.sort(Comparator.comparing(Product::getPrice));
  arrayList.forEach(System.out::println);


  System.out.println("subList");
  arrayList.subList(0, 2).forEach(System.out::println);


  System.out.println("arrayList.contains(product2): " + arrayList.contains(product2));


  System.out.println("arrayList.isEmpty(): " +  arrayList.isEmpty());

  
  System.out.println("retainAll");
  arrayList.retainAll(arrayList2);
  arrayList.forEach(System.out::println);

  
  System.out.println("arrayList.size(): " + arrayList.size());

  
  System.out.println("toArray");
  Object[] array = arrayList.toArray();
  Arrays.stream(array).forEach(System.out::println);

  
  arrayList.clear();
 }
}
