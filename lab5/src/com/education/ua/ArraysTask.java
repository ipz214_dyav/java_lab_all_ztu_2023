package com.education.ua;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ArraysTask {
    public static void main(String[] args) {

        Product[] products = new Product[3];
        Product[] copyProducts = new Product[3];
        Product product1 = new Product("������", 18.50, "���������");
        Product product2 = new Product("���", 21.20, "���������");
        Product product3 = new Product("�������", 16.50, "���������");


        products[0] = product1;
        products[1] = product2;
        products[2] = product3;


        List<Product> list = Arrays.asList(products);
        List<Product> copyList = Arrays.asList(copyProducts);


        Collections.sort(list);
        list.forEach(System.out::println);


        System.out.println("������ product2 �� ��������� binarySearch: " + Collections.binarySearch(list, product2));


        System.out.println("����� reverse: ");
        Collections.reverse(list);
        list.forEach(System.out::println);


        Collections.shuffle(list);
        System.out.println("����� shuffling: ");
        list.forEach(System.out::println);

        System.out.println("Max: " + Collections.max(list));
        System.out.println("Min: " + Collections.min(list));

    
        System.out.println("Rotate: ");
        Collections.rotate(list, 2);
        list.forEach(System.out::println);

        Collections.fill(list, product1);
        System.out.println("����� fill(list, product1): ");
        list.forEach(System.out::println);

        System.out.println("CopyList:");
        Collections.copy(copyList, list);
        copyList.forEach(System.out::println);

        Collection<Product> checkList = Collections.checkedCollection(list, Product.class);
        System.out.println("Checked list content: " + checkList);

        System.out.println(Collections.frequency(list, product1));
    }
}
