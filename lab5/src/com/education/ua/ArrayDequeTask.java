package com.education.ua;

import java.util.*;

public class ArrayDequeTask {
    public static void main(String[] args) {

        ArrayDeque<Product> queue = new ArrayDeque<>();

        Product product1 = new Product("������", 16.50, "���������");
        Product product2 = new Product("���", 23.10, "���������");
        Product product3 = new Product("�������", 19.40, "���������");


        queue.push(product1);
        queue.push(product2);

    
        queue.offerLast(product3);

        System.out.println("�������� ��������� �����:");
        queue.forEach(System.out::println);

        System.out.println("queue.getFirst(): " + queue.getFirst());

       
        System.out.println("queue.peekLast(): " + queue.peekLast());

        System.out.println("queue.pop(): " + queue.pop());

        System.out.println("�������� ����� ���� queue.pop():");
        queue.forEach(System.out::println);

       
        System.out.println("queue.removeLast(): " + queue.removeLast());

       
        System.out.println("queue.peek(): " + queue.peek());

       
        System.out.println("�������� ����� ���� queue.peek ():");
        queue.forEach(System.out::println);
    }
}
