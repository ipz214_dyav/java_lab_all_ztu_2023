package com.education.ua;

import java.util.HashMap;
import java.util.Map;

public class HashMapTask {
    public static void main(String[] args) {
        
        HashMap<String, Product> hashMap = new HashMap<>();

        Product product1 = new Product("������", 18.50, "��������");
        Product product2 = new Product("���", 21.20, "��������");
        Product product3 = new Product("�������", 16.50, "��������");

        hashMap.put(product1.getName(), product1);
        hashMap.put(product2.getName(), product2);
        hashMap.put(product3.getName(), product3);

        System.out.println("hashMap.get(\"���\"): " + hashMap.get("���"));

        System.out.println("hashMap.containsKey(\"�������\"): " + hashMap.containsKey("�������"));

        System.out.println("hashMap.containsKey(\"���\"): " + hashMap.containsKey("���"));

        System.out.println("hashMap.containsValue(product1): " + hashMap.containsValue(product1));

        System.out.println("hashMap.keySet(): " + hashMap.keySet());

        hashMap.clear();

        hashMap.putIfAbsent(product1.getName(), product1);
        hashMap.putIfAbsent(product2.getName(), product1);

        
        System.out.println("hashMap.values(): " + hashMap.values());

        HashMap<String, Product> hashMap2 = new HashMap<>();

        hashMap2.put(product3.getName(), product3);

        hashMap.putAll(hashMap2);

        hashMap.remove(product2.getName(), product1);

        System.out.println("hashMap.size(): " + hashMap.size());

        for (Map.Entry<String, Product> entry : hashMap.entrySet()) {
            String key = entry.getKey();
            Product value = entry.getValue();
            System.out.println(key +" "+ value);
            
            entry.setValue(product3);
        }

        System.out.println(hashMap.values());
    }
}
